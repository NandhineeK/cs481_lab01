
import 'dart:async';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:internalization/FadeAnimation.dart';

void main() {

  runApp(
    EasyLocalization(
      supportedLocales: [Locale('en','US'),Locale('es','US'),Locale('fr','FA')],
      path: 'assets',
      fallbackLocale:Locale('en','US'),
      child: MaterialApp(home: MyApp()),

    ),

  );

}

class MyApp extends StatefulWidget {
  @override
  _State createState() => new _State();
}

enum Answers{YES,NO,MAYBE}

//State is information of the application that can change over time or when some actions are taken.
class _State extends State<MyApp>{

  String _value = '';

  void _setValue(String value) => setState(() => _value = value);

  Future _askUser() async {
    switch(
    await showDialog(
        context: context,
        /*it shows a popup with few options which you can select, for option we
        created enums which we can use with switch statement, in this first switch
        will wait for the user to select the option which it can use with switch cases*/
        child: new SimpleDialog(
          title: new Text('Do you like This Travel App?').tr(),
          children: <Widget>[
            new SimpleDialogOption(child: new Text('Yes!!!').tr(),onPressed: (){
              Navigator.pop(context, Answers.YES);
              },),
            new SimpleDialogOption(child: new Text('NO :(').tr(),onPressed: (){
              Navigator.pop(context, Answers.NO);
              },),
            new SimpleDialogOption(child: new Text('Maybe :|').tr(),onPressed: (){
              Navigator.pop(context, Answers.MAYBE);
              },),
          ],
        )
    )
    ) {
      case Answers.YES:
        _setValue('Yes!!!!! We are glad you like it!!');
        break;
      case Answers.NO:
        _setValue('No:( We will try our best to improve.');
        break;
      case Answers.MAYBE:
        _setValue('Maybe? This is confusing.');
        break;
    }
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      supportedLocales: context.supportedLocales,
      localizationsDelegates: context.localizationDelegates,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        backgroundColor: Colors.cyan,

        appBar: AppBar(
          title: Text("Yoda's Travel"),
          centerTitle: true,
          backgroundColor: Colors.indigo,
          elevation: 0,
        ),

        body:
        ListView(
        children: <Widget>[

          SizedBox(height:5),
              Center(
                child: Image.asset('assets/collage.jpg',
                  fit: BoxFit.cover,
                  height: 300,
                  width: 400,
                ),
              ),
          SizedBox(height:15),
              Center(
                child:FadeAnimation(.10, Text('Welcome to Travel Explore',textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.indigo,
                      fontSize: 30, fontWeight: FontWeight.bold),).tr()),
                ),
          SizedBox(height:10),
              Center(
                child: Text('Plan your trip quick and easy with us.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ).tr(),
              ),

          SizedBox(height:15),
              Center(
                child: Text('Select Language.', textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.indigo,
                    fontFamily: 'Pacifico',
                    fontSize: 20,
                  ),
                ).tr(),
              ),
              SizedBox(height:10),
              RaisedButton(
                onPressed: () {
                  context.locale=Locale('en','US');
                },
                child: Text("English",
                  style: TextStyle(
                    color: Colors.white,
                  ),).tr(),
                color: Colors.indigo,
              ),
              SizedBox(height: 15),
              RaisedButton(
                onPressed: () {
                  context.locale=Locale('es','US');
                },
                child: Text("Spanish",
                  style: TextStyle(
                    color: Colors.white,
                  ),).tr(),
                color: Colors.indigo,
              ),
              SizedBox(height: 15),

              RaisedButton(
                onPressed: () {
                  context.locale=Locale('fr','FA');
                },
                child: Text("French",
                style: TextStyle(
                  color: Colors.white,
                ),).tr(),
                color: Colors.indigo,
              ),
           Container(
            padding:  EdgeInsets.all(32.0),
            child:  Center(
              child:  Column(

                children: <Widget>[
                   Text(_value,
                style: TextStyle(
                  color: Colors.white,
                ),

                ).tr(),
                  RaisedButton(onPressed: _askUser,
                    child: new Text('Let us know?',
                    style: TextStyle(
                        color: Colors.white,
                    ),).tr(),

                    color: Colors.redAccent,)
                ],
              ),

            ),
          ),
            ],
        ),

      ),

    );
  }
}

